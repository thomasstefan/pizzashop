package at.fhv.pizzashop.web.rest;

import at.fhv.pizzashop.PizzashopApp;
import at.fhv.pizzashop.domain.Pizza;
import at.fhv.pizzashop.repository.PizzaRepository;
import at.fhv.pizzashop.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static at.fhv.pizzashop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PizzaResource} REST controller.
 */
@SpringBootTest(classes = PizzashopApp.class)
public class PizzaResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Double DEFAULT_PRICE = 1D;
    private static final Double UPDATED_PRICE = 2D;

    @Autowired
    private PizzaRepository pizzaRepository;

    @Mock
    private PizzaRepository pizzaRepositoryMock;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPizzaMockMvc;

    private Pizza pizza;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PizzaResource pizzaResource = new PizzaResource(pizzaRepository);
        this.restPizzaMockMvc = MockMvcBuilders.standaloneSetup(pizzaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pizza createEntity(EntityManager em) {
        Pizza pizza = new Pizza()
            .name(DEFAULT_NAME)
            .price(DEFAULT_PRICE);
        return pizza;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pizza createUpdatedEntity(EntityManager em) {
        Pizza pizza = new Pizza()
            .name(UPDATED_NAME)
            .price(UPDATED_PRICE);
        return pizza;
    }

    @BeforeEach
    public void initTest() {
        pizza = createEntity(em);
    }

    @Test
    @Transactional
    public void createPizza() throws Exception {
        int databaseSizeBeforeCreate = pizzaRepository.findAll().size();

        // Create the Pizza
        restPizzaMockMvc.perform(post("/api/pizzas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pizza)))
            .andExpect(status().isCreated());

        // Validate the Pizza in the database
        List<Pizza> pizzaList = pizzaRepository.findAll();
        assertThat(pizzaList).hasSize(databaseSizeBeforeCreate + 1);
        Pizza testPizza = pizzaList.get(pizzaList.size() - 1);
        assertThat(testPizza.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPizza.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    public void createPizzaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pizzaRepository.findAll().size();

        // Create the Pizza with an existing ID
        pizza.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPizzaMockMvc.perform(post("/api/pizzas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pizza)))
            .andExpect(status().isBadRequest());

        // Validate the Pizza in the database
        List<Pizza> pizzaList = pizzaRepository.findAll();
        assertThat(pizzaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = pizzaRepository.findAll().size();
        // set the field null
        pizza.setName(null);

        // Create the Pizza, which fails.

        restPizzaMockMvc.perform(post("/api/pizzas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pizza)))
            .andExpect(status().isBadRequest());

        List<Pizza> pizzaList = pizzaRepository.findAll();
        assertThat(pizzaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = pizzaRepository.findAll().size();
        // set the field null
        pizza.setPrice(null);

        // Create the Pizza, which fails.

        restPizzaMockMvc.perform(post("/api/pizzas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pizza)))
            .andExpect(status().isBadRequest());

        List<Pizza> pizzaList = pizzaRepository.findAll();
        assertThat(pizzaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPizzas() throws Exception {
        // Initialize the database
        pizzaRepository.saveAndFlush(pizza);

        // Get all the pizzaList
        restPizzaMockMvc.perform(get("/api/pizzas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pizza.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllPizzasWithEagerRelationshipsIsEnabled() throws Exception {
        PizzaResource pizzaResource = new PizzaResource(pizzaRepositoryMock);
        when(pizzaRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restPizzaMockMvc = MockMvcBuilders.standaloneSetup(pizzaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restPizzaMockMvc.perform(get("/api/pizzas?eagerload=true"))
        .andExpect(status().isOk());

        verify(pizzaRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllPizzasWithEagerRelationshipsIsNotEnabled() throws Exception {
        PizzaResource pizzaResource = new PizzaResource(pizzaRepositoryMock);
            when(pizzaRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restPizzaMockMvc = MockMvcBuilders.standaloneSetup(pizzaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restPizzaMockMvc.perform(get("/api/pizzas?eagerload=true"))
        .andExpect(status().isOk());

            verify(pizzaRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getPizza() throws Exception {
        // Initialize the database
        pizzaRepository.saveAndFlush(pizza);

        // Get the pizza
        restPizzaMockMvc.perform(get("/api/pizzas/{id}", pizza.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pizza.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPizza() throws Exception {
        // Get the pizza
        restPizzaMockMvc.perform(get("/api/pizzas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePizza() throws Exception {
        // Initialize the database
        pizzaRepository.saveAndFlush(pizza);

        int databaseSizeBeforeUpdate = pizzaRepository.findAll().size();

        // Update the pizza
        Pizza updatedPizza = pizzaRepository.findById(pizza.getId()).get();
        // Disconnect from session so that the updates on updatedPizza are not directly saved in db
        em.detach(updatedPizza);
        updatedPizza
            .name(UPDATED_NAME)
            .price(UPDATED_PRICE);

        restPizzaMockMvc.perform(put("/api/pizzas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPizza)))
            .andExpect(status().isOk());

        // Validate the Pizza in the database
        List<Pizza> pizzaList = pizzaRepository.findAll();
        assertThat(pizzaList).hasSize(databaseSizeBeforeUpdate);
        Pizza testPizza = pizzaList.get(pizzaList.size() - 1);
        assertThat(testPizza.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPizza.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void updateNonExistingPizza() throws Exception {
        int databaseSizeBeforeUpdate = pizzaRepository.findAll().size();

        // Create the Pizza

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPizzaMockMvc.perform(put("/api/pizzas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pizza)))
            .andExpect(status().isBadRequest());

        // Validate the Pizza in the database
        List<Pizza> pizzaList = pizzaRepository.findAll();
        assertThat(pizzaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePizza() throws Exception {
        // Initialize the database
        pizzaRepository.saveAndFlush(pizza);

        int databaseSizeBeforeDelete = pizzaRepository.findAll().size();

        // Delete the pizza
        restPizzaMockMvc.perform(delete("/api/pizzas/{id}", pizza.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pizza> pizzaList = pizzaRepository.findAll();
        assertThat(pizzaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
