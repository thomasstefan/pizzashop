package at.fhv.pizzashop.web.rest;

import at.fhv.pizzashop.PizzashopApp;
import at.fhv.pizzashop.domain.Topping;
import at.fhv.pizzashop.repository.ToppingRepository;
import at.fhv.pizzashop.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static at.fhv.pizzashop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ToppingResource} REST controller.
 */
@SpringBootTest(classes = PizzashopApp.class)
public class ToppingResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Double DEFAULT_PRICE = 1D;
    private static final Double UPDATED_PRICE = 2D;

    @Autowired
    private ToppingRepository toppingRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restToppingMockMvc;

    private Topping topping;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ToppingResource toppingResource = new ToppingResource(toppingRepository);
        this.restToppingMockMvc = MockMvcBuilders.standaloneSetup(toppingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Topping createEntity(EntityManager em) {
        Topping topping = new Topping()
            .name(DEFAULT_NAME)
            .price(DEFAULT_PRICE);
        return topping;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Topping createUpdatedEntity(EntityManager em) {
        Topping topping = new Topping()
            .name(UPDATED_NAME)
            .price(UPDATED_PRICE);
        return topping;
    }

    @BeforeEach
    public void initTest() {
        topping = createEntity(em);
    }

    @Test
    @Transactional
    public void createTopping() throws Exception {
        int databaseSizeBeforeCreate = toppingRepository.findAll().size();

        // Create the Topping
        restToppingMockMvc.perform(post("/api/toppings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(topping)))
            .andExpect(status().isCreated());

        // Validate the Topping in the database
        List<Topping> toppingList = toppingRepository.findAll();
        assertThat(toppingList).hasSize(databaseSizeBeforeCreate + 1);
        Topping testTopping = toppingList.get(toppingList.size() - 1);
        assertThat(testTopping.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTopping.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    public void createToppingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = toppingRepository.findAll().size();

        // Create the Topping with an existing ID
        topping.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restToppingMockMvc.perform(post("/api/toppings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(topping)))
            .andExpect(status().isBadRequest());

        // Validate the Topping in the database
        List<Topping> toppingList = toppingRepository.findAll();
        assertThat(toppingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = toppingRepository.findAll().size();
        // set the field null
        topping.setName(null);

        // Create the Topping, which fails.

        restToppingMockMvc.perform(post("/api/toppings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(topping)))
            .andExpect(status().isBadRequest());

        List<Topping> toppingList = toppingRepository.findAll();
        assertThat(toppingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = toppingRepository.findAll().size();
        // set the field null
        topping.setPrice(null);

        // Create the Topping, which fails.

        restToppingMockMvc.perform(post("/api/toppings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(topping)))
            .andExpect(status().isBadRequest());

        List<Topping> toppingList = toppingRepository.findAll();
        assertThat(toppingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllToppings() throws Exception {
        // Initialize the database
        toppingRepository.saveAndFlush(topping);

        // Get all the toppingList
        restToppingMockMvc.perform(get("/api/toppings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(topping.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getTopping() throws Exception {
        // Initialize the database
        toppingRepository.saveAndFlush(topping);

        // Get the topping
        restToppingMockMvc.perform(get("/api/toppings/{id}", topping.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(topping.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTopping() throws Exception {
        // Get the topping
        restToppingMockMvc.perform(get("/api/toppings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTopping() throws Exception {
        // Initialize the database
        toppingRepository.saveAndFlush(topping);

        int databaseSizeBeforeUpdate = toppingRepository.findAll().size();

        // Update the topping
        Topping updatedTopping = toppingRepository.findById(topping.getId()).get();
        // Disconnect from session so that the updates on updatedTopping are not directly saved in db
        em.detach(updatedTopping);
        updatedTopping
            .name(UPDATED_NAME)
            .price(UPDATED_PRICE);

        restToppingMockMvc.perform(put("/api/toppings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTopping)))
            .andExpect(status().isOk());

        // Validate the Topping in the database
        List<Topping> toppingList = toppingRepository.findAll();
        assertThat(toppingList).hasSize(databaseSizeBeforeUpdate);
        Topping testTopping = toppingList.get(toppingList.size() - 1);
        assertThat(testTopping.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTopping.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void updateNonExistingTopping() throws Exception {
        int databaseSizeBeforeUpdate = toppingRepository.findAll().size();

        // Create the Topping

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restToppingMockMvc.perform(put("/api/toppings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(topping)))
            .andExpect(status().isBadRequest());

        // Validate the Topping in the database
        List<Topping> toppingList = toppingRepository.findAll();
        assertThat(toppingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTopping() throws Exception {
        // Initialize the database
        toppingRepository.saveAndFlush(topping);

        int databaseSizeBeforeDelete = toppingRepository.findAll().size();

        // Delete the topping
        restToppingMockMvc.perform(delete("/api/toppings/{id}", topping.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Topping> toppingList = toppingRepository.findAll();
        assertThat(toppingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
