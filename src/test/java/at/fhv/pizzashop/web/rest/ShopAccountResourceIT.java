package at.fhv.pizzashop.web.rest;

import at.fhv.pizzashop.PizzashopApp;
import at.fhv.pizzashop.domain.ShopAccount;
import at.fhv.pizzashop.repository.ShopAccountRepository;
import at.fhv.pizzashop.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static at.fhv.pizzashop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShopAccountResource} REST controller.
 */
@SpringBootTest(classes = PizzashopApp.class)
public class ShopAccountResourceIT {

    private static final LocalDate DEFAULT_CREATED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private ShopAccountRepository shopAccountRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShopAccountMockMvc;

    private ShopAccount shopAccount;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShopAccountResource shopAccountResource = new ShopAccountResource(shopAccountRepository);
        this.restShopAccountMockMvc = MockMvcBuilders.standaloneSetup(shopAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopAccount createEntity(EntityManager em) {
        ShopAccount shopAccount = new ShopAccount()
            .created(DEFAULT_CREATED);
        return shopAccount;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopAccount createUpdatedEntity(EntityManager em) {
        ShopAccount shopAccount = new ShopAccount()
            .created(UPDATED_CREATED);
        return shopAccount;
    }

    @BeforeEach
    public void initTest() {
        shopAccount = createEntity(em);
    }

    @Test
    @Transactional
    public void createShopAccount() throws Exception {
        int databaseSizeBeforeCreate = shopAccountRepository.findAll().size();

        // Create the ShopAccount
        restShopAccountMockMvc.perform(post("/api/shop-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopAccount)))
            .andExpect(status().isCreated());

        // Validate the ShopAccount in the database
        List<ShopAccount> shopAccountList = shopAccountRepository.findAll();
        assertThat(shopAccountList).hasSize(databaseSizeBeforeCreate + 1);
        ShopAccount testShopAccount = shopAccountList.get(shopAccountList.size() - 1);
        assertThat(testShopAccount.getCreated()).isEqualTo(DEFAULT_CREATED);
    }

    @Test
    @Transactional
    public void createShopAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shopAccountRepository.findAll().size();

        // Create the ShopAccount with an existing ID
        shopAccount.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShopAccountMockMvc.perform(post("/api/shop-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopAccount)))
            .andExpect(status().isBadRequest());

        // Validate the ShopAccount in the database
        List<ShopAccount> shopAccountList = shopAccountRepository.findAll();
        assertThat(shopAccountList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllShopAccounts() throws Exception {
        // Initialize the database
        shopAccountRepository.saveAndFlush(shopAccount);

        // Get all the shopAccountList
        restShopAccountMockMvc.perform(get("/api/shop-accounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED.toString())));
    }
    
    @Test
    @Transactional
    public void getShopAccount() throws Exception {
        // Initialize the database
        shopAccountRepository.saveAndFlush(shopAccount);

        // Get the shopAccount
        restShopAccountMockMvc.perform(get("/api/shop-accounts/{id}", shopAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shopAccount.getId().intValue()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingShopAccount() throws Exception {
        // Get the shopAccount
        restShopAccountMockMvc.perform(get("/api/shop-accounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShopAccount() throws Exception {
        // Initialize the database
        shopAccountRepository.saveAndFlush(shopAccount);

        int databaseSizeBeforeUpdate = shopAccountRepository.findAll().size();

        // Update the shopAccount
        ShopAccount updatedShopAccount = shopAccountRepository.findById(shopAccount.getId()).get();
        // Disconnect from session so that the updates on updatedShopAccount are not directly saved in db
        em.detach(updatedShopAccount);
        updatedShopAccount
            .created(UPDATED_CREATED);

        restShopAccountMockMvc.perform(put("/api/shop-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedShopAccount)))
            .andExpect(status().isOk());

        // Validate the ShopAccount in the database
        List<ShopAccount> shopAccountList = shopAccountRepository.findAll();
        assertThat(shopAccountList).hasSize(databaseSizeBeforeUpdate);
        ShopAccount testShopAccount = shopAccountList.get(shopAccountList.size() - 1);
        assertThat(testShopAccount.getCreated()).isEqualTo(UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void updateNonExistingShopAccount() throws Exception {
        int databaseSizeBeforeUpdate = shopAccountRepository.findAll().size();

        // Create the ShopAccount

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShopAccountMockMvc.perform(put("/api/shop-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shopAccount)))
            .andExpect(status().isBadRequest());

        // Validate the ShopAccount in the database
        List<ShopAccount> shopAccountList = shopAccountRepository.findAll();
        assertThat(shopAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShopAccount() throws Exception {
        // Initialize the database
        shopAccountRepository.saveAndFlush(shopAccount);

        int databaseSizeBeforeDelete = shopAccountRepository.findAll().size();

        // Delete the shopAccount
        restShopAccountMockMvc.perform(delete("/api/shop-accounts/{id}", shopAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShopAccount> shopAccountList = shopAccountRepository.findAll();
        assertThat(shopAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
