package at.fhv.pizzashop.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import at.fhv.pizzashop.web.rest.TestUtil;

public class ShopAccountTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopAccount.class);
        ShopAccount shopAccount1 = new ShopAccount();
        shopAccount1.setId(1L);
        ShopAccount shopAccount2 = new ShopAccount();
        shopAccount2.setId(shopAccount1.getId());
        assertThat(shopAccount1).isEqualTo(shopAccount2);
        shopAccount2.setId(2L);
        assertThat(shopAccount1).isNotEqualTo(shopAccount2);
        shopAccount1.setId(null);
        assertThat(shopAccount1).isNotEqualTo(shopAccount2);
    }
}
