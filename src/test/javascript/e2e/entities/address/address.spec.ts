import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import AddressComponentsPage, { AddressDeleteDialog } from './address.page-object';
import AddressUpdatePage from './address-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Address e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let addressComponentsPage: AddressComponentsPage;
  let addressUpdatePage: AddressUpdatePage;
  let addressDeleteDialog: AddressDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
  });

  it('should load Addresses', async () => {
    await navBarPage.getEntityPage('address');
    addressComponentsPage = new AddressComponentsPage();
    expect(await addressComponentsPage.getTitle().getText()).to.match(/Addresses/);
  });

  it('should load create Address page', async () => {
    await addressComponentsPage.clickOnCreateButton();
    addressUpdatePage = new AddressUpdatePage();
    expect(await addressUpdatePage.getPageTitle().getAttribute('id')).to.match(/pizzashopApp.address.home.createOrEditLabel/);
    await addressUpdatePage.cancel();
  });

  it('should create and save Addresses', async () => {
    async function createAddress() {
      await addressComponentsPage.clickOnCreateButton();
      await addressUpdatePage.setStreetInput('street');
      expect(await addressUpdatePage.getStreetInput()).to.match(/street/);
      await addressUpdatePage.setCityInput('city');
      expect(await addressUpdatePage.getCityInput()).to.match(/city/);
      await waitUntilDisplayed(addressUpdatePage.getSaveButton());
      await addressUpdatePage.save();
      await waitUntilHidden(addressUpdatePage.getSaveButton());
      expect(await addressUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createAddress();
    await addressComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await addressComponentsPage.countDeleteButtons();
    await createAddress();

    await addressComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await addressComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Address', async () => {
    await addressComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await addressComponentsPage.countDeleteButtons();
    await addressComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    addressDeleteDialog = new AddressDeleteDialog();
    expect(await addressDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/pizzashopApp.address.delete.question/);
    await addressDeleteDialog.clickOnConfirmButton();

    await addressComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await addressComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
