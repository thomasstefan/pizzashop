import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ToppingComponentsPage, { ToppingDeleteDialog } from './topping.page-object';
import ToppingUpdatePage from './topping-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Topping e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let toppingComponentsPage: ToppingComponentsPage;
  let toppingUpdatePage: ToppingUpdatePage;
  let toppingDeleteDialog: ToppingDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
  });

  it('should load Toppings', async () => {
    await navBarPage.getEntityPage('topping');
    toppingComponentsPage = new ToppingComponentsPage();
    expect(await toppingComponentsPage.getTitle().getText()).to.match(/Toppings/);
  });

  it('should load create Topping page', async () => {
    await toppingComponentsPage.clickOnCreateButton();
    toppingUpdatePage = new ToppingUpdatePage();
    expect(await toppingUpdatePage.getPageTitle().getAttribute('id')).to.match(/pizzashopApp.topping.home.createOrEditLabel/);
    await toppingUpdatePage.cancel();
  });

  it('should create and save Toppings', async () => {
    async function createTopping() {
      await toppingComponentsPage.clickOnCreateButton();
      await toppingUpdatePage.setNameInput('name');
      expect(await toppingUpdatePage.getNameInput()).to.match(/name/);
      await toppingUpdatePage.setPriceInput('5');
      expect(await toppingUpdatePage.getPriceInput()).to.eq('5');
      await waitUntilDisplayed(toppingUpdatePage.getSaveButton());
      await toppingUpdatePage.save();
      await waitUntilHidden(toppingUpdatePage.getSaveButton());
      expect(await toppingUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createTopping();
    await toppingComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await toppingComponentsPage.countDeleteButtons();
    await createTopping();

    await toppingComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await toppingComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Topping', async () => {
    await toppingComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await toppingComponentsPage.countDeleteButtons();
    await toppingComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    toppingDeleteDialog = new ToppingDeleteDialog();
    expect(await toppingDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/pizzashopApp.topping.delete.question/);
    await toppingDeleteDialog.clickOnConfirmButton();

    await toppingComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await toppingComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
