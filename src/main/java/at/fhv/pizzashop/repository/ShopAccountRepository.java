package at.fhv.pizzashop.repository;
import at.fhv.pizzashop.domain.ShopAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ShopAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShopAccountRepository extends JpaRepository<ShopAccount, Long> {

}
