package at.fhv.pizzashop.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Pizza.
 */
@Entity
@Table(name = "pizza")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Pizza implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "price", nullable = false)
    private Double price;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "pizza_topping",
               joinColumns = @JoinColumn(name = "pizza_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "topping_id", referencedColumnName = "id"))
    private Set<Topping> toppings = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("pizzas")
    private Order order;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Pizza name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public Pizza price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Set<Topping> getToppings() {
        return toppings;
    }

    public Pizza toppings(Set<Topping> toppings) {
        this.toppings = toppings;
        return this;
    }

    public Pizza addTopping(Topping topping) {
        this.toppings.add(topping);
        topping.getPizzas().add(this);
        return this;
    }

    public Pizza removeTopping(Topping topping) {
        this.toppings.remove(topping);
        topping.getPizzas().remove(this);
        return this;
    }

    public void setToppings(Set<Topping> toppings) {
        this.toppings = toppings;
    }

    public Order getOrder() {
        return order;
    }

    public Pizza order(Order order) {
        this.order = order;
        return this;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pizza)) {
            return false;
        }
        return id != null && id.equals(((Pizza) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Pizza{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
