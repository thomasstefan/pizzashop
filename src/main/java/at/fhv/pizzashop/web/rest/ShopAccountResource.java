package at.fhv.pizzashop.web.rest;

import at.fhv.pizzashop.domain.ShopAccount;
import at.fhv.pizzashop.repository.ShopAccountRepository;
import at.fhv.pizzashop.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link at.fhv.pizzashop.domain.ShopAccount}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ShopAccountResource {

    private final Logger log = LoggerFactory.getLogger(ShopAccountResource.class);

    private static final String ENTITY_NAME = "shopAccount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShopAccountRepository shopAccountRepository;

    public ShopAccountResource(ShopAccountRepository shopAccountRepository) {
        this.shopAccountRepository = shopAccountRepository;
    }

    /**
     * {@code POST  /shop-accounts} : Create a new shopAccount.
     *
     * @param shopAccount the shopAccount to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shopAccount, or with status {@code 400 (Bad Request)} if the shopAccount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shop-accounts")
    public ResponseEntity<ShopAccount> createShopAccount(@RequestBody ShopAccount shopAccount) throws URISyntaxException {
        log.debug("REST request to save ShopAccount : {}", shopAccount);
        if (shopAccount.getId() != null) {
            throw new BadRequestAlertException("A new shopAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShopAccount result = shopAccountRepository.save(shopAccount);
        return ResponseEntity.created(new URI("/api/shop-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shop-accounts} : Updates an existing shopAccount.
     *
     * @param shopAccount the shopAccount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shopAccount,
     * or with status {@code 400 (Bad Request)} if the shopAccount is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shopAccount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shop-accounts")
    public ResponseEntity<ShopAccount> updateShopAccount(@RequestBody ShopAccount shopAccount) throws URISyntaxException {
        log.debug("REST request to update ShopAccount : {}", shopAccount);
        if (shopAccount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShopAccount result = shopAccountRepository.save(shopAccount);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shopAccount.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shop-accounts} : get all the shopAccounts.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shopAccounts in body.
     */
    @GetMapping("/shop-accounts")
    public List<ShopAccount> getAllShopAccounts() {
        log.debug("REST request to get all ShopAccounts");
        return shopAccountRepository.findAll();
    }

    /**
     * {@code GET  /shop-accounts/:id} : get the "id" shopAccount.
     *
     * @param id the id of the shopAccount to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shopAccount, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shop-accounts/{id}")
    public ResponseEntity<ShopAccount> getShopAccount(@PathVariable Long id) {
        log.debug("REST request to get ShopAccount : {}", id);
        Optional<ShopAccount> shopAccount = shopAccountRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(shopAccount);
    }

    /**
     * {@code DELETE  /shop-accounts/:id} : delete the "id" shopAccount.
     *
     * @param id the id of the shopAccount to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shop-accounts/{id}")
    public ResponseEntity<Void> deleteShopAccount(@PathVariable Long id) {
        log.debug("REST request to delete ShopAccount : {}", id);
        shopAccountRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
