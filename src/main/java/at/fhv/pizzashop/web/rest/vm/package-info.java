/**
 * View Models used by Spring MVC REST controllers.
 */
package at.fhv.pizzashop.web.rest.vm;
