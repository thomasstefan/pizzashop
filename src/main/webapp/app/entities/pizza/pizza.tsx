import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './pizza.reducer';
import { IPizza } from 'app/shared/model/pizza.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPizzaProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Pizza extends React.Component<IPizzaProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { pizzaList, match } = this.props;
    return (
      <div>
        <h2 id="pizza-heading">
          <Translate contentKey="pizzashopApp.pizza.home.title">Pizzas</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="pizzashopApp.pizza.home.createLabel">Create a new Pizza</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          {pizzaList && pizzaList.length > 0 ? (
            <Table responsive aria-describedby="pizza-heading">
              <thead>
                <tr>
                  <th>
                    <Translate contentKey="global.field.id">ID</Translate>
                  </th>
                  <th>
                    <Translate contentKey="pizzashopApp.pizza.name">Name</Translate>
                  </th>
                  <th>
                    <Translate contentKey="pizzashopApp.pizza.price">Price</Translate>
                  </th>
                  <th>
                    <Translate contentKey="pizzashopApp.pizza.topping">Topping</Translate>
                  </th>
                  <th>
                    <Translate contentKey="pizzashopApp.pizza.order">Order</Translate>
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {pizzaList.map((pizza, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${pizza.id}`} color="link" size="sm">
                        {pizza.id}
                      </Button>
                    </td>
                    <td>{pizza.name}</td>
                    <td>{pizza.price}</td>
                    <td>
                      {pizza.toppings
                        ? pizza.toppings.map((val, j) => (
                            <span key={j}>
                              <Link to={`topping/${val.id}`}>{val.id}</Link>
                              {j === pizza.toppings.length - 1 ? '' : ', '}
                            </span>
                          ))
                        : null}
                    </td>
                    <td>{pizza.order ? <Link to={`order/${pizza.order.id}`}>{pizza.order.id}</Link> : ''}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${pizza.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${pizza.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${pizza.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">
              <Translate contentKey="pizzashopApp.pizza.home.notFound">No Pizzas found</Translate>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ pizza }: IRootState) => ({
  pizzaList: pizza.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Pizza);
