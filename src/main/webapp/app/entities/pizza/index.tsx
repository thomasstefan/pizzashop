import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Pizza from './pizza';
import PizzaDetail from './pizza-detail';
import PizzaUpdate from './pizza-update';
import PizzaDeleteDialog from './pizza-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PizzaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PizzaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PizzaDetail} />
      <ErrorBoundaryRoute path={match.url} component={Pizza} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={PizzaDeleteDialog} />
  </>
);

export default Routes;
