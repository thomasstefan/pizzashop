import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './shop-account.reducer';
import { IShopAccount } from 'app/shared/model/shop-account.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IShopAccountDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ShopAccountDetail extends React.Component<IShopAccountDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { shopAccountEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="pizzashopApp.shopAccount.detail.title">ShopAccount</Translate> [<b>{shopAccountEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="created">
                <Translate contentKey="pizzashopApp.shopAccount.created">Created</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={shopAccountEntity.created} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="pizzashopApp.shopAccount.user">User</Translate>
            </dt>
            <dd>{shopAccountEntity.user ? shopAccountEntity.user.id : ''}</dd>
            <dt>
              <Translate contentKey="pizzashopApp.shopAccount.address">Address</Translate>
            </dt>
            <dd>{shopAccountEntity.address ? shopAccountEntity.address.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/shop-account" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/shop-account/${shopAccountEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ shopAccount }: IRootState) => ({
  shopAccountEntity: shopAccount.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShopAccountDetail);
