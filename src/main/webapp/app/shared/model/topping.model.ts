import { IPizza } from 'app/shared/model/pizza.model';

export interface ITopping {
  id?: number;
  name?: string;
  price?: number;
  pizzas?: IPizza[];
}

export const defaultValue: Readonly<ITopping> = {};
